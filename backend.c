#define _LARGEFILE64_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<linux/fs.h>
#include<ext2fs/ext2_fs.h>
#include<unistd.h>
#include<sys/stat.h>
#include<errno.h>
#include<string.h>
#include<time.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<sys/mman.h>

#ifndef HAVE___U32
#define HAVE___U32
#ifdef __U32_TYPEDEF
typedef __U32_TYPEDEF __u32;
#else   
#if (4 == 4)
typedef unsigned int    __u32;
#else   
#if (8 == 4)
typedef unsigned long   __u32;
#else   
#if (2 == 4)
typedef unsigned short  __u32;   
#else
#undef HAVE___U32
 ?== error: undefined 32 bit type
#endif /* SIZEOF_SHORT == 4 */
#endif /* SIZEOF_LONG == 4 */
#endif /* SIZEOF_INT == 4 */
#endif /* __U32_TYPEDEF */
#endif /* HAVE___U32 */

#ifndef HAVE___U16
#define HAVE___U16
#ifdef __U16_TYPEDEF
typedef __U16_TYPEDEF __u16;
#else
#if (4 == 2)
typedef unsigned int    __u16;
#else
#if (2 == 2)
typedef unsigned short  __u16;
#else
#undef HAVE___U16
  ?==error: undefined 16 bit type
#endif /* SIZEOF_SHORT == 2 */
#endif /* SIZEOF_INT == 2 */
#endif /* __U16_TYPEDEF */
#endif /* HAVE___U16 */

#ifndef HAVE___U8
#define HAVE___U8
#ifdef __U8_TYPEDEF
typedef __U8_TYPEDEF __u8;
#else
typedef unsigned char __u8;
#endif
#endif /* HAVE___U8 */

#ifndef HAVE___S8
#define HAVE___S8
#ifdef __S8_TYPEDEF
typedef __S8_TYPEDEF __s8;
#else
typedef signed char __s8;
#endif
#endif /* HAVE___S8 */


typedef struct{

        __u16   s_magic;                /* Magic signature */
        char    s_last_mounted[64];  /* directory last mounted on */
        __u32   s_inodes_count;         /* Inodes count */
        __u32   s_blocks_count;         /* Blocks count */
        __u32   s_r_blocks_count;       /* Reserved blocks count */
        __u32   s_free_blocks_count;    /* Free blocks count */
        __u32   s_free_inodes_count;    /* Free inodes count */
        __u32   s_first_data_block;     /* First Data Block */
        __u32   s_block_size;       /* Block size */
        __u32   s_cluster_size;     /* Allocation cluster size */
        __u32   s_blocks_per_group;     /* # Blocks per group */
        __u32   s_clusters_per_group;   /* # Fragments per group */
        __u32   s_inodes_per_group;     /* # Inodes per group */
        __u32   s_mnt_count;            /* Mount count */
	    __u32   s_checkinterval;        /* max. time between checks */

}superblock_data;

struct ext2_super_block sb;
struct ext2_group_desc bgdesc;
struct ext2_inode inodes;
struct ext2_dir_entry_2 dirent;
int count;                                             // used when reading
unsigned int bsize;	                               // used to store the block size.
int start;                                            // Used for indirection.
int end;                                              // Used for indirection.
int indirection_flag;                               // Used for checking whether to print start, end.
int data_flag;
int range_flag = 1;          // A global variable to check whether we have to print the range or not.

// A function used to read the contents of the superblock.
void read_superblock(struct ext2_super_block sb){ 

	int shm_fd;
	superblock_data * data;
	void * ptr;

	// Create shared memory object
    	shm_fd = shm_open("shared_mem", O_CREAT | O_RDWR, 0666);
    	if (shm_fd == -1) {
        	printf("Shared memory failed\n");
        	exit(1);
    	}

	 // Configure the size of the shared memory object
   	 ftruncate(shm_fd, 4096); // size of superblock is 4096

	// Attach the shared memory object to the current address space
	ptr = mmap(0, 4096 , PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    	if (ptr == MAP_FAILED) {
        	printf("Map failed\n");
        	exit(1);
       	}

	data = (superblock_data*) ptr;
	data->s_magic = sb.s_magic;//H
	strcpy(data->s_last_mounted,sb.s_last_mounted);//s
	data->s_inodes_count = sb.s_inodes_count;//I
	data->s_blocks_count = sb.s_blocks_count;//I
	data->s_r_blocks_count = sb.s_r_blocks_count;//I
	data->s_free_blocks_count = sb.s_free_blocks_count;//I
	data->s_free_inodes_count = sb.s_free_inodes_count;//I
	data->s_first_data_block = sb.s_first_data_block;//I
	data->s_block_size = 1024 << sb.s_log_block_size;//I
	data->s_cluster_size = 1024 << sb.s_log_cluster_size;//I
	data->s_blocks_per_group = sb.s_blocks_per_group;//I
	data->s_clusters_per_group = sb.s_clusters_per_group;//I
	data->s_inodes_per_group = sb.s_inodes_per_group;//I
	data->s_mnt_count = sb.s_mnt_count;//I
	data->s_checkinterval = sb.s_checkinterval;//I

}

// A function to read the nth block group descriptor
void read_blockgroup(int fd,int n){

	printf(" BLOCK GROUP %d\n",n);

	lseek64(fd,4096 + n*sizeof(struct ext2_group_desc),SEEK_SET);
	count = read(fd,&bgdesc,sizeof(struct ext2_group_desc));

	printf("Block Bitmap : %u\n",bgdesc.bg_block_bitmap);
	printf("Inode Bitmap : %u\n",bgdesc.bg_inode_bitmap);
	printf("Inode Table : %u\n",bgdesc.bg_inode_table);
	printf("Free Blocks Count : %u\n",bgdesc.bg_free_blocks_count);
	printf("Free Inodes Count : %u\n",bgdesc.bg_free_inodes_count);
	printf("Number Of Used Directories : %u\n",bgdesc.bg_used_dirs_count);
	printf("Unused Inodes Count : %u\n",bgdesc.bg_itable_unused);

}

// Function used for single indirection.
// We use one more range_flag. When set to 1 means we want to print the range else we want to print the entire thing.
void single_indirection(int fd, int inode, int data_flag){

	lseek64(fd,inode*bsize,SEEK_SET);            // We have jumped to the respective inode.
	unsigned int address;                        
        
	// To know how many entries are there in our block, we divide the block size with the size an inode number takes.
	count= read(fd,&address,sizeof(unsigned int));
	if(address != 0) 
		start = address;

	for(int i=0;i<(bsize/sizeof(unsigned int));i++){            
		if(address!=0){
		        if(range_flag==1)                      	
				end = address;
			else{
				printf("%u\n",address);
			}
		}
		else
			break;
		count = read(fd,&address,sizeof(unsigned int));    // Go to the next address.
	}
	if(data_flag==0 && range_flag==1){
			printf("%d - %d\n",start,end);
	}
	else{
		if(range_flag==0)
			return;
		char data[bsize*(start-end)];
		for(int i=start;i<=end;i++){
                	lseek64(fd,i*bsize,SEEK_SET);
                	count = read(fd,&data,bsize);
                	for(int i=0; i<bsize; i++)
                    		printf("%c",data[i]);
		}
	}
}

// Function used for double indirection.
void double_indirection(int fd, int inode, int data_flag){
       
	lseek64(fd,inode*bsize,SEEK_SET);
	unsigned int address;
        
	count = read(fd,&address,sizeof(unsigned int));
	for(int i=0;i<(bsize/sizeof(unsigned int));i++){
		if(address!=0)
		     single_indirection(fd,address,data_flag);
		else
		     break;           

		// In this case address points to a block which is a single indirect block.
		lseek64(fd,inode*bsize + (i+1)*sizeof(unsigned int),SEEK_SET);
		// The above line is to get back the file pointer to where it should belong as single indirection function changes the file pointer.
	        count = read(fd,&address,sizeof(unsigned int));
	}

}

// Function used for triple indirection.
void triple_indirection(int fd, int inode,int data_flag){

	lseek64(fd,inode*bsize,SEEK_SET);
	unsigned int address;
	
	count = read(fd,&address,sizeof(unsigned int));
	for(int i=0;i<(bsize/sizeof(unsigned int));i++){
		if(address!=0)
			double_indirection(fd,address,data_flag);
		else
			break;
		lseek64(fd,inode*bsize + (i+1)*sizeof(unsigned int),SEEK_SET);
		// The above line is to get back the file pointer to where it should belong as double and in turn single 
		// indirection function changes the file pointer.
		count = read(fd,&address,sizeof(unsigned int));
	}
} 

// This function returns the inode offset required to lseek given the inode
unsigned long long inode_offset(int fd, int g_inode){
	
	    int bgno;
        unsigned long long idx;
        unsigned long long inode_off;

        bgno =  (g_inode-1)/sb.s_inodes_per_group;
        // Now we have which block group contains the inode.

        idx = (g_inode-1)%sb.s_inodes_per_group;
        // Now we have the index of the given inode in that group.

	//lseek64(fd,4096,SEEK_SET);
        lseek64(fd,bsize + bgno*sizeof(bgdesc),SEEK_SET);
        count = read(fd,&bgdesc,sizeof(struct ext2_group_desc));

        unsigned long long calc1 = (unsigned long long)(bgdesc.bg_inode_table) *( unsigned long long)(bsize);
        unsigned long long calc2 = (unsigned long long)(idx) * (unsigned long long)(sb.s_inode_size);
        inode_off = calc1 + calc2;
	
	return inode_off;

}


// This function is used to print the inode data blocks given the inode number.
// g_inode means given inode.
void print_inode(int fd, int g_inode){          

	unsigned long long inode_off = inode_offset(fd,g_inode);
	lseek64(fd,inode_off,SEEK_SET);      
	// Now we have reached our inode.
    count = read(fd,&inodes,sizeof(struct ext2_inode));

	// Now we are going to read the blocks of the inode.
	for(int i=0; i<15; i++){

	    if(inodes.i_block[i]==0)
	       break;

	    if(i<12){
	       		printf("%u\n",inodes.i_block[i]);
		}
	    else if(i==12){
	       printf("Single indirection\n");
               single_indirection(fd,inodes.i_block[12],0);
            }

	    else if(i==13){
		  printf("Double indirection\n");
		  double_indirection(fd,inodes.i_block[13],0);
	    }

	    else if(i==14){
		    printf("Triple indirection\n");
	 	    triple_indirection(fd,inodes.i_block[14],0);
            }
	}

}

// This function is used to print the data of the block.
void print_data(int fd, int inode_no,int data_block){
	
	unsigned long long inode_off = inode_offset(fd,inode_no);
	lseek64(fd,inode_off,SEEK_SET); 
	count = read(fd,&inodes,sizeof(struct ext2_inode));

	char * type_str;
	uint16_t type = inodes.i_mode;
	type = (type & 0xF000) >> 12;
	switch (type) {
										
				case 0x8:
					type_str = "Regular";
					break;
				case 0x4:
					type_str = "Directory";
					break;
				default:
					type_str = "Unknown";
					break;
						}
	if(strcmp(type_str,"Directory")==0){

		for(int i=0;i<inodes.i_blocks/8;i++){

		lseek64(fd, inodes.i_block[i]*bsize, SEEK_SET);
		do{
			count = read(fd,&dirent,sizeof(struct ext2_dir_entry_2));
			printf("%u   %u   %u   %u   %s\n",dirent.inode,dirent.rec_len,dirent.name_len,dirent.file_type,dirent.name);
			lseek64(fd,dirent.rec_len - sizeof(struct ext2_dir_entry_2),SEEK_CUR);
		}while(strlen(dirent.name)!=0);

	  }
	}
	else if(strcmp(type_str,"Regular")==0){
		lseek64(fd,data_block*bsize,SEEK_SET);
		char data[4096];
			// lseek64(fd,inodes.i_block[i]*bsize,SEEK_SET);
			count = read(fd,&data,bsize);
			for(int i=0; i<bsize; i++)
				printf("%c",data[i]);
	}
}

// Printing the binary format.
void pbinary(unsigned char byte){

	for(int i=7;i>=0;i--)
		printf("%d", (byte >> i) & 1);

}

// This function prints the block bitmap.
void block_bitmap(int fd, int bno){

	lseek64(fd, 4096 + bno*sizeof(struct ext2_group_desc),SEEK_SET);
	count = read(fd,&bgdesc,sizeof(struct ext2_group_desc));
	// Have read the block group descriptor now.
	
	unsigned char * bitmap;
	bitmap = (char*)malloc(bsize);
	lseek64(fd,bgdesc.bg_block_bitmap * bsize,SEEK_SET);
	read(fd,bitmap,bsize);
	// Now we read the block bitmap.
	
	// The number of blocks the bitmap can refer to is 8(1 byte is made up fo 8 bits)*bsize
		
	for(int i=0;i<bsize;i++){
		pbinary(bitmap[i]);    
		printf(" ");
		if( (i+1) % 8 == 0) printf("\n");
	}
	
	free(bitmap);


}

// This function prints the inode bitmap
void inode_bitmap(int fd, int bno){

	lseek64(fd, 4096 + bno*sizeof(struct ext2_group_desc),SEEK_SET);
	count = read(fd,&bgdesc,sizeof(struct ext2_group_desc));

	unsigned char * bitmap;
	bitmap = (char*)malloc(bsize);
	lseek64(fd,bgdesc.bg_inode_bitmap * bsize, SEEK_SET);
	read(fd,bitmap,bsize);
	// Now we read the inode bitmap.
	
	for(int i=0;i<bsize;i++){
		pbinary(bitmap[i]);
		printf(" ");
		if( (i+1) % 8 == 0) printf("\n");
	}

	free(bitmap);

}

// A function to read the inode table
// The data block flag is used as we are trying to retrieve the number of used data blocks and we dont want to print anything there.
void inode_table(int fd, int bno, int data_block_flag){

		unsigned int first_inode = bno * sb.s_inodes_per_group;
		unsigned int cur_inode;
		char * type_str;

        for(int i=0; i<sb.s_inodes_per_group; i++){
				cur_inode = first_inode + i + 1;
				unsigned long long inode_off = inode_offset(fd,cur_inode);
				lseek64(fd,inode_off,SEEK_SET);      
				// Now we have reached our inode.
				count = read(fd,&inodes,sizeof(struct ext2_inode));		
				uint16_t type = inodes.i_mode;
				type = (type & 0xF000) >> 12;
				switch (type) {
													
							case 0x8:
								type_str = "Regular";
								break;
							case 0x4:
								type_str = "Directory";
								break;
							default:
								type_str = "Unknown";
								break;
									}
						
						// printf("Type:%s",type_str);
				
				printf("%u %s %o %u %d %u\n",cur_inode,type_str,inodes.i_mode,inodes.i_size,inodes.i_links_count,inodes.i_blocks/8);
        }	      
}	

//Returning the number of block groups.
int no_of_bgroups(){

       printf("%d\n",sb.s_blocks_count/sb.s_blocks_per_group);
}

int main(int argc, char * argv[]){

       int fd = open(argv[1],O_RDONLY);
       if(fd==-1){
               perror("Error in opening file!");
               exit(errno);
       }
      
       lseek64(fd,1024,SEEK_SET);
       // As after the first 1024 bytes comes the contents of the superblock.
       count = read(fd,&sb,sizeof(struct ext2_super_block));

       bsize = 1024 << sb.s_log_block_size;


	if(strcmp(argv[2],"no_of_bgroups")==0){
			no_of_bgroups();
			return 0;
	}
	else if(strcmp(argv[2],"read_superblock")==0){
			read_superblock(sb);
			return 0;
	}
	else if(strcmp(argv[3],"read_blockgroup")==0){
			read_blockgroup(fd,atoi(argv[2]));
			return 0;
	}
	else if(strcmp(argv[3],"block_bitmap")==0){
			block_bitmap(fd,atoi(argv[2]));
			return 0;
	}
	else if(strcmp(argv[3],"inode_bitmap")==0){
			inode_bitmap(fd,atoi(argv[2]));
			return 0;
	}
	else if(strcmp(argv[3],"inode_table")==0){
			inode_table(fd,atoi(argv[2]),0);
			return 0;
	}
	else if(strcmp(argv[3],"print_inode")==0){
			print_inode(fd,atoi(argv[2]));
			return 0;
	}
	else if(strcmp(argv[4],"print_data")==0){
			print_data(fd,atoi(argv[2]),atoi(argv[3]));
			return 0;
	}
	else
		return 1;

       close(fd);
       return 0;
}

