from subprocess import PIPE
import subprocess
from PyQt5.QtGui import QFont, QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QApplication, QDialog, QTreeView, QTextEdit, QWidget, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout, QPushButton, QScrollArea, QStyledItemDelegate
from PyQt5.QtCore import Qt
import pyudev
import mmap
import struct


# This is the first window that displays the ext2 partitions on your system.
class MyWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):

        # Creating the top level layout.
        main_layout = QVBoxLayout()

        # Create a label and set its text and alignment properties
        heading = QLabel("DEBUGFS", self)
        heading.setAlignment(Qt.AlignHCenter)

        # Setting the font and its size
        font = QFont('Arial', 40, QFont.Bold)
        heading.setFont(font)

        # Set color of the heading.
        heading.setStyleSheet('color: orange;')

        # Add the label to a vertical layout
        main_layout.addWidget(heading)

        # Set the background color to black
        self.setStyleSheet("background-color: black")

        # Create a context object for accessing the system's devices
        context = pyudev.Context()

        # Create a list of all block devices on the system
        devices = list(context.list_devices(subsystem="block"))
    
        # Create a list of ext2 partitions
        ext2_partitions = [device.get("DEVNAME") for device in devices if device.get("ID_FS_TYPE") == "ext2"]
        # ext2_partitions = []
        count = 0
        # Store the number of ext2 partitions and their names
        if(len(ext2_partitions)==0):
            empty = QLabel('NO EXT2 PARTITIONS PRESENT ON THE SYSTEM')
            empty.setAlignment(Qt.AlignHCenter)
            empty.setStyleSheet('color:orange')
            font = QFont('Arial',50,QFont.Bold)
            empty.setFont(font)
            main_layout.addWidget(empty)

        else:
            count = len(ext2_partitions);
    
            #Create a grid layout for the buttons
            grid_layout = QGridLayout()

            # Add the buttons to the grid layout
            for row in range(len(ext2_partitions)):
                for col in range(2):
                    index = row * 2 + col
                    if index < len(ext2_partitions):
                        button = QPushButton(ext2_partitions[index])
                        # Now if the button is clicked it will execute the function open_partition.

                        button.setFixedSize(300,100)
                        button.setStyleSheet("color : black;")
                        button.setStyleSheet("background-color : orange;")
                        font = QFont('Arial', 20, QFont.Bold)
                        button.setFont(font)
                        grid_layout.addWidget(button, row, col)

#To avoid the problem of only the last button text being sent to the on_button_clicked function we have saved the text of the button at the time the lambda function is defined so that each button connects to the correct text.
                        button.clicked.connect(lambda _, text=ext2_partitions[index]: self.open_partition(text))

        # Set the layout for the window
            main_layout.addLayout(grid_layout)
        
        # Add the label to a vertical layout
            main_layout.addWidget(heading)
        
        #Set the layout for the main window
        self.setLayout(main_layout)
        
        # Set window properties
        self.setGeometry(100, 100, 400, 300)
        self.showMaximized()
        self.setWindowTitle('Debugfs-GUI')
        self.show()

    def open_partition(self,p_name):
            self.close()               # Closes the current window
            window2 = Window2(p_name)        # Opens a new window
            window2.show()             # Shows the new window

# The function used to create the table of block groups.
def create_button_grid(self,p_name,buttons_per_row, num_buttons):
        button_grid_widget = QWidget()
        button_grid_layout = QVBoxLayout()

        # Now we calculate the required number of rows.
        button_rows = num_buttons // buttons_per_row + (num_buttons % buttons_per_row > 0)

        for i in range(button_rows):
            button_row = QHBoxLayout()           # We give each rows layout.

            for j in range(buttons_per_row):
                # Calculating the button_num that is the block group number.
                button_num = i * buttons_per_row + j    

                if button_num < num_buttons:
                    button_text = f"Block Group {button_num}"
                    button = QPushButton(button_text)
                    button.setStyleSheet('background-color: orange')
                    button_row.addWidget(button)   # Adding the button to the row.

                    button.clicked.connect(lambda _, text=button_text: open_bgroup(self,p_name,text))



            # Adding the button row to the button grid layout.
            button_grid_layout.addLayout(button_row)
        # Adding the button_grid_layout
        button_grid_widget.setLayout(button_grid_layout)

        scroll_area = QScrollArea()
        # This ensures that the widget automatically resize according to the window.
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(button_grid_widget)

        return scroll_area

def open_bgroup(self,p_name,bg_name):
    self.close()
    window3 = Window3(p_name,bg_name)
    window3.show()



# The class whose object is created when any of the partitions are opened.
# This will be the block groups window.
class Window2(QWidget):
    def __init__(self,p_name):
        super().__init__()
        self.wind2(p_name)

    def wind2(self,p_name):
        self.setWindowTitle('Debugfs-GUI')
        layout = QVBoxLayout()

        #Maximizing the window
        self.showMaximized()
        
        #background colour setting
        self.setStyleSheet("background-color: black")

        # Create a button that opens the old window again
        back_button = QPushButton('BACK', clicked = lambda: self.open_window1())
        back_button.setStyleSheet("background-color: orange")
        back_button.setFixedSize(50,30)

        # Created the boot sector button
        bootsector_button = QPushButton('BOOT SECTOR',clicked = lambda: self.open_popup())
        bootsector_button.setStyleSheet('background-color: orange')

        #calling the corresponding c function to get the number of block groups.
        result = subprocess.run(['sudo','./backend' ,p_name,'no_of_bgroups'],stdout=subprocess.PIPE)

        #stored the number of block groups in the selected partition.
        count = result.stdout.decode('utf-8') 
        # Calling the function that creates our table.
        button_grid = create_button_grid(self,p_name,4,int(count)+1)
        # The reason we typecasted count as integer is because the value returned would be in the form of string.

        layout.addWidget(back_button)         # We added the back_button to the layout.
        layout.addWidget(bootsector_button)   # We added the boot_sector button to the layout.
        layout.addWidget(button_grid)         # Adding the button grid to the layout.
        self.setLayout(layout)                # Set the layout.

    def open_window1(self):
        self.close() # Close the current window
        window1 = MyWindow()                # Go back to our main window.
        window1.show()
    

    def open_popup(self):
    # Create the popup window
        popup = QDialog(self)
        popup.setWindowTitle("Boot Sector File")
        popup.setGeometry(150, 150, 200, 100)

        # Add some text to the popup
        label = QLabel("Warning! Opening this file can damage your system.")
        label.setStyleSheet('background-color: orange; color: black')
        popup_layout = QVBoxLayout()
        popup_layout.addWidget(label)
        popup.setLayout(popup_layout)

        # Show the popup window
        popup.exec_()

# This will be the window for the block group contents.
class Window3(QWidget):
    def __init__(self,p_name,bg_name):
        super().__init__()
        self.wind3(p_name,bg_name)

    def wind3(self,p_name,bg_name):
        self.setWindowTitle("Debugfs-GUI")
        layout = QVBoxLayout()

        self.showMaximized()

        self.setStyleSheet('background-color: black')
        
        back_button = QPushButton('BACK', clicked = lambda: self.open_window2(p_name))
        back_button.setStyleSheet('background-color: orange')
        back_button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        back_button.setFont(font)

        layout.addWidget(back_button)

        layout.setAlignment(Qt.AlignHCenter)
        
        button1 = QPushButton('SUPERBLOCK', clicked = lambda: self.open_superblock(p_name,bg_name))
        button1.setStyleSheet('background-color: orange')
        button1.setFixedSize(600,100)
        layout.addWidget(button1)

        button2 = QPushButton('GROUP DESCRIPTORS', clicked = lambda : self.open_bg(p_name,bg_name))
        button2.setStyleSheet('background-color: orange')
        button2.setFixedSize(600,100)
        layout.addWidget(button2)

        button3 = QPushButton('BLOCK BITMAP', clicked = lambda : self.b_bitmap(p_name,bg_name))
        button3.setStyleSheet('background-color: orange')
        button3.setFixedSize(600,100)
        layout.addWidget(button3)

        button4 = QPushButton('INODE BITMAP', clicked = lambda : self.i_bitmap(p_name,bg_name))
        button4.setStyleSheet('background-color: orange')
        button4.setFixedSize(600,100)
        layout.addWidget(button4)

        button5 = QPushButton('INODE TABLE', clicked = lambda : self.i_table(p_name,bg_name))
        button5.setStyleSheet('background-color: orange')
        button5.setFixedSize(600,100)
        layout.addWidget(button5)

        button6 = QPushButton('DATA BLOCKS')
        button6.setEnabled(False)
        button6.setStyleSheet('background-color: orange; color: black')
        button6.setFixedSize(600,100)
        layout.addWidget(button6)

        self.setLayout(layout)
    
    def open_window2(self,p_name):
        self.close()
        window2 = Window2(p_name)
        window2.show()

    def open_superblock(self,p_name,bg_name):
        self.close()
        window4 = Window4(p_name,bg_name)
        window4.show()

    def open_bg(self,p_name,bg_name):
        self.close()
        window5 = Window5(p_name,bg_name)
        window5.show()

    def b_bitmap(self,p_name,bg_name):
        self.close()
        window6 = Window6(p_name,bg_name)
        window6.show()
    
    def i_bitmap(self,p_name,bg_name):
        self.close()
        window7 = Window7(p_name,bg_name)
        window7.show()

    def i_table(self,p_name,bg_name):
        self.close()
        window8 = Window8(p_name,bg_name)
        window8.show()


# This is the window to be displayed when we click on the superblock.
class Window4(QWidget):
    def __init__(self,p_name,bg_name):
        super().__init__()
        self.wind4(p_name,bg_name)

    def wind4(self,p_name,bg_name):
        self.setWindowTitle('DEBUGFS_GUI')
        layout = QVBoxLayout()

        self.showMaximized()

        self.setStyleSheet("background-color: black")

        button = QPushButton('BACK', clicked = lambda: self.open_window3(p_name,bg_name))
        button.setStyleSheet('background-color: orange')
        button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        button.setFont(font)
        layout.addWidget(button)
        
        # Create a QTextEdit widget to display the output.
        textEdit = QTextEdit(self)

        # Calling the corresponding c function to print the contents of the superblock.
        result = subprocess.run(['sudo','./backend',p_name,'read_superblock'],stdout=subprocess.PIPE)
        #result = Popen(['sudo','./backend','p_name','read_superblock'],stdout=PIPE)
        if result.returncode == 0:
            output = result.stdout.decode('utf-8')

        heading = QLabel("SUPERBLOCK")
        heading.setAlignment(Qt.AlignHCenter)
        font = QFont('Arial', 40, QFont.Bold)
        heading.setFont(font)
        heading.setStyleSheet('color : orange')
        

        self.shm_fd = open("/dev/shm/shared_mem", "r+b")
        self.sb = mmap.mmap(self.shm_fd.fileno(), 0)

        magic_number = QLabel()
        last_mounted = QLabel()
        mkfs_time = QLabel()
        mount_time = QLabel()
        write_time = QLabel()
        inodes_count = QLabel()
        blocks_count = QLabel()
        reserved_blocks= QLabel()
        free_blocks= QLabel()
        free_inodes= QLabel()
        first_data_block= QLabel()
        block_size= QLabel()
        cluster_size = QLabel()
        blocks_per_group= QLabel()
        cluster_per_group= QLabel()
        inodes_per_group= QLabel()
        mount_count = QLabel()
        check_interval = QLabel()
        
        layout.addWidget(heading)
        layout.addWidget(magic_number)
        layout.addWidget(last_mounted)
        layout.addWidget(inodes_count)
        layout.addWidget(blocks_count)
        layout.addWidget(reserved_blocks)
        layout.addWidget(free_blocks)
        layout.addWidget(free_inodes)
        layout.addWidget(first_data_block)
        layout.addWidget(block_size)
        layout.addWidget(cluster_size)
        layout.addWidget(blocks_per_group)
        layout.addWidget(cluster_per_group)
        layout.addWidget(inodes_per_group)
        layout.addWidget(mount_count)
        layout.addWidget(check_interval)

        struct_fmt = "H7s10IIIIIIIII10III" #This is the format of contents being sent

        struct_size = struct.calcsize(struct_fmt)
        sb = struct.unpack(struct_fmt, self.sb[:struct_size])
        print(sb[2])
    
        # Update GUI elements
        magic_number.setText("Magic Number : " + hex(sb[0]))
        magic_number.setStyleSheet('background-color:orange')
        font = QFont('Arial', 20, QFont.Bold)
        magic_number.setFont(font)

        last_mounted.setText("Last Mounted : " + (sb[1]).decode('utf-8'))
        last_mounted.setStyleSheet('background-color:orange')
        font = QFont('Arial', 20, QFont.Bold)
        last_mounted.setFont(font)

        inodes_count.setText("Total number of Inodes :" + str(sb[19]))
        inodes_count.setStyleSheet('background-color:orange')
        font = QFont('Arial', 20, QFont.Bold)
        inodes_count.setFont(font)

        blocks_count.setText("Total number of Blocks : "+ str(sb[20]))
        blocks_count.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        blocks_count.setFont(font)

        reserved_blocks.setText("Total number of reserved Blocks : "+ str(sb[21]))
        reserved_blocks.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        reserved_blocks.setFont(font)

        free_blocks.setText("Total number of free Blocks : "+ str(sb[22]))
        free_blocks.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        free_blocks.setFont(font)

        free_inodes.setText("Total number of free inodes : "+ str(sb[23]))
        free_inodes.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        free_inodes.setFont(font)

        first_data_block.setText("First data block : "+ str(sb[24]))
        first_data_block.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        first_data_block.setFont(font)

        block_size.setText("Size of the block : "+ str(sb[25]))
        block_size.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        block_size.setFont(font)

        cluster_size.setText("Fragment Size : "+ str(sb[26]))
        cluster_size.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        cluster_size.setFont(font)

        blocks_per_group.setText("Blocks per group : "+ str(sb[27]))
        blocks_per_group.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        blocks_per_group.setFont(font)

        cluster_per_group.setText("Fragments per group : "+ str(sb[28]))
        cluster_per_group.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        cluster_per_group.setFont(font)

        inodes_per_group.setText("Inodes per group : "+ str(sb[29]))
        inodes_per_group.setStyleSheet('background-color: orange')
        font = QFont('Arial', 20, QFont.Bold)
        inodes_per_group.setFont(font)

        mount_count.setText("Mount Count :" + str(sb[30]))
        mount_count.setStyleSheet('background-color:orange')
        font = QFont('Arial', 20, QFont.Bold)
        mount_count.setFont(font)

        check_interval.setText("Check Interval :" + str(sb[31]))
        check_interval.setStyleSheet('background-color:orange')
        font = QFont('Arial', 20, QFont.Bold)
        check_interval.setFont(font)

        self.sb.close()
        self.shm_fd.close()
        self.setLayout(layout)
        self.show()

    def open_window3(self,p_name,bg_name):
        self.close()
        window3 = Window3(p_name,bg_name)
        window3.show()

# This is the window to be displayed when we click on a block group
class Window5(QWidget):
    def __init__(self,p_name,bg_name):
        super().__init__()
        self.wind5(p_name,bg_name)

    def wind5(self,p_name,bg_name):
        self.setWindowTitle('DEBUGFS_GUI')
        layout = QVBoxLayout()

        self.showMaximized()

        self.setStyleSheet('background-color : black')

        button = QPushButton('BACK', clicked = lambda: self.open_window3(p_name,bg_name))
        button.setStyleSheet('background-color: orange')
        button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        button.setFont(font)
        layout.addWidget(button)

        # Create a QTextEdit widget to display the output.
        textEdit = QTextEdit(self)
        
        bg_no = bg_name.split()[2]
        # Calling the corresponding c function to print the contents of the blockgroup descriptor.
        result = subprocess.run(['sudo','./backend',p_name,bg_no,'read_blockgroup'],stdout=subprocess.PIPE)
        if result.returncode == 0:
            output = result.stdout.decode('utf-8')
        
        output_list = output.split('\n')

        heading = QLabel(output_list[0])
        heading.setStyleSheet('color:orange')
        heading.setAlignment(Qt.AlignHCenter)
        font = QFont('Arial',40,QFont.Bold)
        heading.setFixedSize(1810,150)
        heading.setFont(font)

        block_bitmap = QPushButton(output_list[1], clicked = lambda : self.b_bitmap(p_name,bg_name))
        font = QFont('Arial',25,QFont.Bold)
        block_bitmap.setFont(font)
        block_bitmap.setFixedSize(1810,75)
        block_bitmap.setStyleSheet('text-align:left ; background-color:orange')

        inode_bitmap = QPushButton(output_list[2], clicked = lambda : self.i_bitmap(p_name,bg_name))
        font = QFont('Arial',25,QFont.Bold)
        inode_bitmap.setFont(font)
        inode_bitmap.setFixedSize(1810,75)
        inode_bitmap.setStyleSheet('text-align:left;background-color:orange')

        inode_table = QPushButton(output_list[3], clicked = lambda : self.i_table(p_name,bg_name))
        font = QFont('Arial',25,QFont.Bold)
        inode_table.setFont(font)
        inode_table.setFixedSize(1810,75)
        inode_table.setStyleSheet('text-align:left;background-color:orange')

        free_blocks = QPushButton(output_list[4])
        free_blocks.setEnabled(False)
        font = QFont('Arial',25,QFont.Bold)
        free_blocks.setFixedSize(1810,75)
        free_blocks.setFont(font)
        free_blocks.setStyleSheet('text-align:left ; background-color:orange ; color:black')

        free_inodes = QPushButton(output_list[5])
        free_inodes.setEnabled(False)
        font = QFont('Arial',25,QFont.Bold)
        free_inodes.setFixedSize(1810,75)
        free_inodes.setFont(font)
        free_inodes.setStyleSheet('text-align:left ; background-color:orange ; color:black')

        used_dir = QPushButton(output_list[6])
        used_dir.setEnabled(False)
        font = QFont('Arial',25,QFont.Bold)
        used_dir.setFixedSize(1810,75)
        used_dir.setFont(font)
        used_dir.setStyleSheet('text-align:left ; background-color:orange ; color:black')

        unused = QPushButton(output_list[7])
        unused.setEnabled(False)
        font = QFont('Arial',25,QFont.Bold)
        unused.setFont(font)
        unused.setFixedSize(1810,75)
        unused.setStyleSheet('text-align:left ; background-color:orange ; color:black')

        layout.addWidget(heading)
        layout.addWidget(block_bitmap)
        layout.addWidget(inode_bitmap)
        layout.addWidget(inode_table)
        layout.addWidget(free_blocks)
        layout.addWidget(free_inodes)
        layout.addWidget(used_dir)
        layout.addWidget(unused)
        self.setLayout(layout)

    def open_window3(self,p_name,bg_name):
        self.close()
        window3 = Window3(p_name,bg_name)
        window3.show()
    
    def b_bitmap(self,p_name,bg_name):
        self.close()
        window6 = Window6(p_name,bg_name)
        window6.show()

    def i_bitmap(self,p_name,bg_name):
        self.close()
        window7 = Window7(p_name,bg_name)
        window7.show()
    
    def i_table(self,p_name,bg_name):
        self.close()
        window8 = Window8(p_name,bg_name)
        window8.show()

def create_button_grid2(self,buttons_per_row, num_buttons,main_list):
        button_grid_widget = QWidget()
        button_grid_layout = QVBoxLayout()

        # Now we calculate the required number of rows.
        button_rows = num_buttons // buttons_per_row + (num_buttons % buttons_per_row > 0)
        k = 0
        for i in range(button_rows):
            button_row = QHBoxLayout()           # We give each rows layout.

            for j in range(buttons_per_row):
                button_num = i * buttons_per_row + j    

                if button_num < num_buttons:
                    button_text = main_list[k]
                    k+=1
                    button = QPushButton(button_text)
                    button.setStyleSheet('background-color: orange')
                    button_row.addWidget(button)   # Adding the button to the row.

            # Adding the button row to the button grid layout.
            button_grid_layout.addLayout(button_row)
        # Adding the button_grid_layout
        button_grid_widget.setLayout(button_grid_layout)

        scroll_area = QScrollArea()
        # This ensures that the widget automatically resize according to the window.
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(button_grid_widget)

        return scroll_area


#This is the window to be displayed when clicked on the block bitmap.
class Window6(QWidget):
    def __init__(self,p_name,bg_name):
        super().__init__()
        self.wind6(p_name,bg_name)

    def wind6(self,p_name,bg_name):
        self.setWindowTitle('DEBUGFS_GUI')
        layout = QVBoxLayout()

        self.showMaximized()

        self.setStyleSheet('background-color : black')

        button = QPushButton('BACK', clicked = lambda: self.open_window3(p_name,bg_name))
        button.setStyleSheet('background-color: orange')
        button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        button.setFont(font)
        layout.addWidget(button)

        heading = QLabel('BLOCK BITMAP')
        heading.setStyleSheet('color:orange')
        heading.setAlignment(Qt.AlignHCenter)
        font = QFont('Arial',40,QFont.Bold)
        heading.setFixedSize(1810,100)
        heading.setFont(font)
        layout.addWidget(heading)

        bg_no = bg_name.split()[2]
        # Calling the corresponding c function to print the contents of the block bitmap.
        result = subprocess.run(['sudo','./backend',p_name,bg_no,'block_bitmap'],stdout=subprocess.PIPE)
        if result.returncode == 0:
            output = result.stdout.decode('utf-8')
        
        output_list = output.split('\n')
        temp_list = []
        main_list = []
        for subpart in output_list:
            for temp_subpart in subpart.split():
                temp_list.append(temp_subpart)
        
        for value in temp_list:
            l = len(value)
            for i in range(l):
                main_list.append(value[i])
        
        button_grid = create_button_grid2(self,20,len(main_list)-27000,main_list)

        layout.addWidget(button_grid)
        self.setLayout(layout)

    def open_window3(self,p_name,bg_name):
        self.close()
        window3 = Window3(p_name,bg_name)
        window3.show()

# This is the window to be displayed when we click on the inode bitmap button.
class Window7(QWidget):
    def __init__(self,p_name,bg_name):
        super().__init__()
        self.wind7(p_name,bg_name)

    def wind7(self,p_name,bg_name):
        self.setWindowTitle('DEBUGFS_GUI')
        layout = QVBoxLayout()

        self.showMaximized()

        self.setStyleSheet('background-color : black')

        button = QPushButton('BACK', clicked = lambda: self.open_window3(p_name,bg_name))
        button.setStyleSheet('background-color: orange')
        button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        button.setFont(font)
        layout.addWidget(button)

        heading = QLabel('INODE BITMAP')
        heading.setStyleSheet('color:orange')
        heading.setAlignment(Qt.AlignHCenter)
        font = QFont('Arial',40,QFont.Bold)
        heading.setFixedSize(1810,100)
        heading.setFont(font)
        layout.addWidget(heading)

        bg_no = bg_name.split()[2]
        # Calling the corresponding c function to print the contents of the inode bitmap.
        result = subprocess.run(['sudo','./backend',p_name,bg_no,'inode_bitmap'],stdout=subprocess.PIPE)
        if result.returncode == 0:
            output = result.stdout.decode('utf-8')
        
        output_list = output.split('\n')
        temp_list = []
        main_list = []
        for subpart in output_list:
            for temp_subpart in subpart.split():
                temp_list.append(temp_subpart)
        
        for value in temp_list:
            l = len(value)
            for i in range(l):
                main_list.append(value[i])
        
        button_grid = create_button_grid2(self,20,len(main_list)-27000,main_list)

        layout.addWidget(button_grid)
        self.setLayout(layout)

    def open_window3(self,p_name,bg_name):
        self.close()
        window3 = Window3(p_name,bg_name)
        window3.show()

class PushButtonDelegate(QStyledItemDelegate):
    def __init__(self,parent,selfs,p_name,bg_name):
        super().__init__(parent)
        self.p_name = p_name
        self.selfs = selfs
        self.bg_name = bg_name

    def createEditor(self, parent, option, index):
        if index.column() == 0:
            button = QPushButton(parent, clicked = lambda: self.show_inode(self.selfs,self.p_name,self.bg_name,index.data()))
            button.setText(index.data())
            
            return button
        else:
            return super().createEditor(parent, option, index)

    def setModelData(self, editor, model, index):
        if index.column() == 0:
            model.setData(index, index.data())
        else:
            super().setModelData(editor, model, index)

    def show_inode(self,selfs,p_name,bg_name,inode_no):
        selfs.close()
        window10 = Window10(inode_no,p_name,bg_name)
        window10.show()


#This is the window to be displayed when we click on the inode table
class Window8(QWidget):
    def __init__(self,p_name,bg_name):
        super().__init__()
        self.wind8(p_name,bg_name)

    def wind8(self,p_name,bg_name):
        self.setWindowTitle('DEBUGFS_GUI')
        layout = QVBoxLayout()

        # self.showMaximized()

        self.setStyleSheet('background-color : orange')

        button = QPushButton('BACK', clicked = lambda: self.open_window3(p_name,bg_name))
        button.setStyleSheet('background-color: orange')
        button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        button.setFont(font)
        layout.addWidget(button)

        bg_no = bg_name.split()[2]
        # Calling the corresponding c function to print the contents of the inode table.
        
        result = subprocess.run(['sudo','./backend',p_name,bg_no,'inode_table'],stdout=subprocess.PIPE)
        if result.returncode == 0:
                output = result.stdout.decode('utf-8')
           
        output_list = output.strip().split('\n')
        inode_data = [row.split() for row in output_list]


        self.tree_view = QTreeView()
        # self.tree_view.setFrameStyle(Qt.SolidLine)
        layout.addWidget(self.tree_view)

        self.model = QStandardItemModel(self)
        self.model.setHorizontalHeaderLabels(["Inode Number","Type","Mode","Size (bytes)","Links Count", "Blocks Count"])

        self.tree_view.header().setStyleSheet("background-color: orange")
        self.tree_view.setModel(self.model)
        root_item = self.model.invisibleRootItem()
        for row in inode_data:
                row_item = [QStandardItem(item) for item in row]
                root_item.appendRow(row_item)

        # Set the font and font size of the output
        button_delegate = PushButtonDelegate(self.tree_view,self,p_name,bg_name)
        # button_delegate.p_name = p_name
        self.tree_view.setItemDelegateForColumn(0, button_delegate)
        font = QFont('Arial',20)
        self.tree_view.setFont(font)
        # self.tree_view.setStyleSheet('color:orange')
        self.tree_view.setColumnWidth(0,300)
        self.tree_view.setColumnWidth(1,300)
        self.tree_view.setColumnWidth(2,300)
        self.tree_view.setColumnWidth(3,300)
        self.tree_view.setColumnWidth(4,300)
        self.tree_view.setColumnWidth(5,300)
        # self.tree_view.resizeColumnToContents(1)

# Set the font and font size of the header labels
        font = QFont('Arial',25,QFont.Bold)
        header = self.tree_view.header()
        header.setFont(font)
        header.setStyleSheet('color:black')

        
        self.setLayout(layout)
        self.tree_view.setStyleSheet('color:orange')
        self.tree_view.setStyleSheet("QTreeView::item { border-bottom: 1px solid white; border-right: 1px solid white}")
        self.showMaximized()

    def open_window3(self,p_name,bg_name):
        self.close()
        window3 = Window3(p_name,bg_name)
        window3.show()

# This is the data blocks
class Window10(QWidget):
    def __init__(self,inode_no,p_name,bg_name):
        super().__init__()
        self.wind10(inode_no,p_name,bg_name)

    def wind10(self,inode_no,p_name,bg_name):
        result = subprocess.run(['sudo','./backend',p_name,inode_no,'print_inode'],stdout=subprocess.PIPE)
        if result.returncode == 0:
            output = result.stdout.decode('utf-8')
        if(output==""):
            self.setStyleSheet('background-color: black')
            self.open_window8(p_name,bg_name)
            self.open_popup1()
        else:  
            self.setWindowTitle("Debugfs-GUI")
            layout = QVBoxLayout()

            self.showMaximized()

            self.setStyleSheet('background-color: black')
            
            back_button = QPushButton('BACK', clicked = lambda: self.open_window8(p_name,bg_name))
            back_button.setStyleSheet('background-color: orange')
            back_button.setFixedSize(50,30)
            font = QFont('Arial',10, QFont.Bold)
            back_button.setFont(font)

            layout.addWidget(back_button)

            # This is in case the number of buttons become a lot
            scroll_area = QScrollArea()         
            scroll_area.setWidgetResizable(True)
            layout.addWidget(scroll_area)

            content_widget = QWidget()
            scroll_area.setWidget(content_widget)

            content_layout = QVBoxLayout()
            content_widget.setLayout(content_layout)
            output_list = output.split('\n')
            if(len(output_list)<=12):    # If there are only direct blocks
                sections = [
                            {'heading':'BLOCKS','buttons': output_list},
                            {'heading':'SINGLE INDIRECTION BLOCKS','buttons':[]},
                            {'heading':'DOUBLE INDIRECTION BLOCKS','buttons':[]},
                            {'heading':'TRIPLE INDIRECTION BLOCKS','buttons':[]}
                ]
            elif(len(output_list)==15):    # If there are blocks till single indirection
                single_indirection_pos = output_list.index('Single indirection')
                output_list1 = output_list[:single_indirection_pos]
                output_list2 = output_list[single_indirection_pos+1:]
                sections = [
                            {'heading':'BLOCKS','buttons': output_list1},
                            {'heading':'SINGLE INDIRECTION BLOCKS','buttons':output_list2},
                            {'heading':'DOUBLE INDIRECTION BLOCKS','buttons':[]},
                            {'heading':'TRIPLE INDIRECTION BLOCKS','buttons':[]}
                ]
            elif(len(output_list)>15):
                try:                    # Will handle the case of triple indirection
                    triple_indirection_pos = output_list.index('Triple indirection')
                    single_indirection_pos = output_list.index('Single indirection')
                    double_indirection_pos = output_list.index('Double indirection')
                    output_list1 = output_list[:single_indirection_pos]
                    output_list2 = output_list[single_indirection_pos+1:double_indirection_pos]
                    output_list3 = output_list[double_indirection_pos+1:triple_indirection_pos]
                    output_list4 = output_list[triple_indirection_pos+1:]
                    sections = [
                            {'heading':'BLOCKS','buttons': output_list1},
                            {'heading':'SINGLE INDIRECTION BLOCKS','buttons':output_list2},
                            {'heading':'DOUBLE INDIRECTION BLOCKS','buttons':output_list3},
                            {'heading':'TRIPLE INDIRECTION BLOCKS','buttons':output_list4}
                ]
                except ValueError:           # Will handle the case of double indirection
                    single_indirection_pos = output_list.index('Single indirection')
                    double_indirection_pos = output_list.index('Double indirection')
                    output_list1 = output_list[:single_indirection_pos]
                    output_list2 = output_list[single_indirection_pos+1:double_indirection_pos]
                    output_list3 = output_list[double_indirection_pos+1:]
                    sections = [
                            {'heading':'BLOCKS','buttons': output_list1},
                            {'heading':'SINGLE INDIRECTION BLOCKS','buttons':output_list2},
                            {'heading':'DOUBLE INDIRECTION BLOCKS','buttons':output_list3},
                            {'heading':'TRIPLE INDIRECTION BLOCKS','buttons':[]}
                ]


            for section in sections:
                if section['buttons']:
                    heading_label = QLabel(section['heading'])
                    font = QFont('Arial',20,QFont.Bold)
                    heading_label.setAlignment(Qt.AlignCenter)
                    heading_label.setFont(font)
                    heading_label.setFixedSize(1810,90)
                    heading_label.setStyleSheet('text-align:center ; background-color:orange')
                    content_layout.addWidget(heading_label)

                    for button_text in section['buttons']:
                        if button_text!= '':
                            button = QPushButton(button_text,clicked = lambda: self.open_window11(inode_no,button_text,p_name,bg_name))
                            button.setStyleSheet('background-color:orange')
                            font = QFont('Arial',15,QFont.Bold)
                            button.setFont(font)
                            content_layout.addWidget(button)
                else:
                    text = section['heading'] + '\n\n' + "There are no blocks"
                    
                    heading_label = QLabel(text)
                    font = QFont('Arial',20,QFont.Bold)
                    heading_label.setAlignment(Qt.AlignCenter)
                    heading_label.setFont(font)
                    heading_label.setFixedSize(1810,300)
                    content_layout.addWidget(heading_label)
                    heading_label.setStyleSheet('text-align:center ; background-color:orange')
                    self.setLayout(layout)

    def open_window8(self,p_name,bg_name):
        self.close() # Close the current window
        window8 = Window8(p_name,bg_name)                # Go back to our main window.
        window8.show()

    def open_popup1(self):
    # Create the popup window
        popup = QDialog(self)
        popup.setWindowTitle("Debugfs-GUI")
        popup.setGeometry(150, 150, 200, 100)

        # Add some text to the popup
        label = QLabel("This inode is unused.")
        label.setStyleSheet('background-color: orange; color: black')
        popup_layout = QVBoxLayout()
        popup_layout.addWidget(label)
        popup.setLayout(popup_layout)

        # Show the popup window
        popup.exec_()


    def open_window11(self,inode_no,button_text,p_name,bg_name):
        self.close()
        window11 = Window11(inode_no,button_text,p_name,bg_name)
        window11.show()

#This window is meant to print the contents of a data block
class Window11(QWidget):
    def __init__(self,inode_no,data_block,p_name,bg_name):
        super().__init__()
        self.wind11(inode_no,data_block,p_name,bg_name)

    def wind11(self,inode_no,data_block,p_name,bg_name):
        self.setWindowTitle("Debugfs-GUI")
        layout = QVBoxLayout()

        self.showMaximized()

        self.setStyleSheet('background-color: black')
        
        back_button = QPushButton('BACK', clicked = lambda: self.open_window10(inode_no,p_name,bg_name))
        back_button.setStyleSheet('background-color: orange')
        back_button.setFixedSize(50,30)
        font = QFont('Arial',10, QFont.Bold)
        back_button.setFont(font)

        label = QLabel('CONTENTS')
        font = QFont('Arial',40, QFont.Bold)
        label.setFont(font)
        label.setAlignment(Qt.AlignHCenter)
        label.setStyleSheet('color:orange')

        layout.addWidget(back_button)
        layout.addWidget(label)
        result = subprocess.run(['sudo','./backend',p_name,inode_no,data_block,'print_data'],stdout=subprocess.PIPE)
        if result.returncode == 0:
            try:
                output = result.stdout.decode('utf-8')
                textEdit = QTextEdit(self)
                textEdit.setText(output)
                textEdit.setStyleSheet('background-color: orange')
                textEdit.setReadOnly(True)
                font = QFont('Arial',30, QFont.Bold)
                textEdit.setFont(font)
            except UnicodeDecodeError:
                output1 = result.stdout.decode('latin-1')
                textEdit = QTextEdit(self)
                textEdit.setText(output1)
                textEdit.setStyleSheet('background-color: orange')
                textEdit.setReadOnly(True)
                font = QFont('Arial',30, QFont.Bold)
                textEdit.setFont(font)
                # invalid_byte_pos = e.start
                # non_encoded_str = decoded_str[:invalid_byte_pos].decode('latin-1')
                # encoded_str = decoded_str[invalid_byte_pos:]

                # decoded_encoded_str = encoded_str.decode('utf-8')
                # decoded_str = non_encoded_str + decoded_encoded_str
            # output = result.stdout.decode('utf-8')
            # print(result.stdout.decode(''))
        layout.addWidget(textEdit)
        self.setLayout(layout)
    
    def open_window10(self,inode_no,p_name,bg_name):
        self.close()
        window10 = Window10(inode_no,p_name,bg_name)
        window10.show()


if __name__ == '__main__':
    app = QApplication([])
    window1 = MyWindow()
    app.exec_()
